# Very basic Flask template project

Quickly set up a basic Flask project for serving static 
files and rendering templates.

Run `init.sh` to create a python3 virtual environment
and install flask.

If you like change the `my_app.py` filename and adjust `.flaskenv`
accordingly.

Run with `source venv/bin/activate` then `flask run`.

